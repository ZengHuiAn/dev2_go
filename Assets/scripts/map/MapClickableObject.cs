﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGK {
	public interface MapClickableObject  {
		void OnClick(GameObject obj);
	}
}
