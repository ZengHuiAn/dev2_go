﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGK {
	public interface MapInteractableObject {
		void Interact(GameObject obj);
	}
}